package com.example.springbootdatajpa.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

@Data
@Entity
@Table(name = "tbl_language", uniqueConstraints = @UniqueConstraint(columnNames = {"key", "locale"}))
public class Language implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "key", length = 100, nullable = false)
    private String key;

    @Column(name = "locale", length = 5, nullable = false)
    private String locale;

    @Column(name = "value", length = 500)
    private String value;

    @Column(name = "created_date", updatable = false)
    private Instant createdDate = Instant.now();

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;
}
