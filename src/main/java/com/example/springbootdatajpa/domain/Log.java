package com.example.springbootdatajpa.domain;

import com.example.springbootdatajpa.constants.enumeration.LogAction;
import com.example.springbootdatajpa.constants.enumeration.LogStatus;
import com.example.springbootdatajpa.constants.enumeration.LogType;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

@Data
@Entity
@Table(name = "tbl_log")
public class Log implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "author", length = 100, nullable = false)
    private String author;

    @Column(name = "time", updatable = false)
    private Instant time = Instant.now();

    @Enumerated(value = EnumType.STRING)
    @Column(name = "type", length = 100, nullable = false)
    private LogType type;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "action", length = 100, nullable = false)
    private LogAction action;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "status", length = 100, nullable = false)
    private LogStatus status;

    @Column(name = "description")
    private String description;

}
