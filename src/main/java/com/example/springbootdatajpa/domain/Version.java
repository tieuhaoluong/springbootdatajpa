package com.example.springbootdatajpa.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

@Data
@Entity
@Table(name = "tbl_version")
public class Version implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "version", length = 100, nullable = false)
    private String version;

    @Column(name = "title", length = 1000, nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "created_date", updatable = false)
    private Instant createdDate = Instant.now();

}
