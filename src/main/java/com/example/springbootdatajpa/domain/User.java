package com.example.springbootdatajpa.domain;

import com.example.springbootdatajpa.constants.enumeration.UserGender;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "tbl_user")
public class User extends AbstractAuditingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "login", length = 100, unique = true, nullable = false)
    private String login;

    @Column(name = "password_hash", length = 256, nullable = false)
    private String password;

    @Column(name = "full_name", length = 100, nullable = false)
    private String fullName;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender", length = 20, nullable = false)
    private UserGender gender;

    @Column(name = "email", length = 200, unique = true)
    private String email;

    @Column(name = "locale_key", length = 10)
    private String localeKey;

    @Column(name = "image_url", length = 256)
    private String imageUrl;

    @Column(name = "is_activated", nullable = false)
    private boolean activated;

    @Column(name = "activation_key", length = 20)
    private String activationKey;

    @Column(name = "activation_date")
    private Instant activationDate;

    @Column(name = "reset_key", length = 20)
    private String resetKey;

    @Column(name = "reset_date")
    private Instant resetDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "tbl_user_authority",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "name")}
    )
    private Set<Authority> authorities = new HashSet<>();

}
