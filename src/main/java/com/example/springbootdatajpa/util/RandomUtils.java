package com.example.springbootdatajpa.util;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Utility class for generating random Strings.
 */
public final class RandomUtils {

    private static final int DEF_COUNT = 20;

    private static final SecureRandom SECURE_RANDOM;

    static {
        SECURE_RANDOM = new SecureRandom();
        SECURE_RANDOM.nextBytes(new byte[64]);
    }

    public static String randomString(int length, StringChar stringChar) {
        if (stringChar == null)
            stringChar = StringChar.AlphaNumeric;

        StringBuilder randomString = new StringBuilder(stringChar.getValue());

        Random random = new Random();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(randomString.length());
            result.append(randomString.charAt(index));
        }
        return result.toString();
    }

    public static String generateRandomAlphanumericString() {
        return randomString(DEF_COUNT, StringChar.AlphaNumeric);
    }

    /**
     * Generate a password.
     *
     * @return the generated password.
     */
    public static String generatePassword() {
        return generateRandomAlphanumericString();
    }

    /**
     * Generate an activation key.
     *
     * @return the generated activation key.
     */
    public static String generateActivationKey() {
        return generateRandomAlphanumericString();
    }

    /**
     * Generate a reset key.
     *
     * @return the generated reset key.
     */
    public static String generateResetKey() {
        return generateRandomAlphanumericString();
    }

    public enum StringChar {
        AlphaUpper("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
        AlphaLower(AlphaUpper.getValue().toLowerCase()),
        Numeric("0123456789"),
        AlphaNumeric(AlphaUpper.getValue() + AlphaLower.getValue() + Numeric.getValue()),
        AlphaNumericUpperCase(AlphaUpper.getValue() + Numeric.getValue()),
        AlphaNumericLowerCase(AlphaLower.getValue() + Numeric.getValue());

        private String value;

        StringChar(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
