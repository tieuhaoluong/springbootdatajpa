package com.example.springbootdatajpa.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.AttributeConverter;

public class EntityConverter<T> implements AttributeConverter<T, String> {
    private static final Logger LOGGER = LoggerFactory.getLogger(EntityConverter.class);

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(T attribute) {
        String value = null;
        try {
            value = mapper.writeValueAsString(attribute);
        } catch (JsonProcessingException e) {
            LOGGER.warn(e.getMessage());
        }
        return value;
    }

    @Override
    public T convertToEntityAttribute(String dbData) {
        T set = null;
        try {
            set = mapper.readValue(dbData, new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            LOGGER.warn(e.getMessage());
        }
        return set;
    }
}
