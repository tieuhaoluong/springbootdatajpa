package com.example.springbootdatajpa.security;

import com.example.springbootdatajpa.repository.UserRepository;
import com.example.springbootdatajpa.util.SecurityUtils;
import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
public class SpringUserDetailsService implements UserDetailsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SpringUserDetailsService.class);

    private final UserRepository userRepository;
    private final EmailValidator emailValidator = new EmailValidator();

    public SpringUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login) {
        LOGGER.debug("Authenticating {}", login);
        if (emailValidator.isValid(login, null)) {
            return userRepository
                    .findByEmailIgnoreCase(login)
                    .map(SecurityUtils::createSpringUser)
                    .orElseThrow(() -> new UsernameNotFoundException("User with email " + login + " was not found in the database"));
        }

        String lowercaseLogin = login.toLowerCase(Locale.ENGLISH);
        return userRepository
                .findByLogin(lowercaseLogin)
                .map(SecurityUtils::createSpringUser)
                .orElseThrow(() -> new UsernameNotFoundException("User " + lowercaseLogin + " was not found in the database"));
    }


}
