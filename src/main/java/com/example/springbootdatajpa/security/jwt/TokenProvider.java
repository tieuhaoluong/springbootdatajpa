package com.example.springbootdatajpa.security.jwt;

import com.example.springbootdatajpa.constants.Constants;
import com.example.springbootdatajpa.domain.User;
import com.example.springbootdatajpa.repository.UserRepository;
import com.example.springbootdatajpa.security.SpringUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class TokenProvider {
    private static final String AUTHORITIES_KEY = "auth";
    private final Logger LOGGER = LoggerFactory.getLogger(TokenProvider.class);
    private final UserRepository userRepository;

    @Value("${app.security.authentication.jwt.secret-key:Y29tLnN0aWQuZG9jdW1lbnRfZGlnaXRpemF0aW9u}")
    private String secretKey;

    @Value("${app.security.authentication.jwt.token-validity-in-seconds:600}")
    private long tokenValidityInSeconds;

    @Value("${app.security.authentication.jwt.token-validity-in-seconds-remember-me:86400}")
    private long tokenValidityInSecondsForRememberMe;

    public TokenProvider(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public JWTToken createToken(Authentication authentication, boolean rememberMe) {
        String authorities = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining(","));

        long now = (new Date()).getTime();
        Date validity;
        if (rememberMe) {
            validity = new Date(now + this.tokenValidityInSecondsForRememberMe * 1000);
        } else {
            validity = new Date(now + this.tokenValidityInSeconds * 1000);
        }

        JWTToken jwtToken = new JWTToken();
        jwtToken.setId(UUID.randomUUID().toString());
        jwtToken.setExpiresAt(validity);

        String token = Jwts.builder()
                .setId(jwtToken.getId())
                .setSubject(authentication.getName())
                .claim(AUTHORITIES_KEY, authorities)
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setExpiration(validity)
                .compact();

        jwtToken.setToken(token);
        return jwtToken;
    }

    public Authentication getAuthentication(String token) {
        Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
        User user = userRepository.findByLogin(claims.getSubject()).orElse(null);
        if (user == null)
            return null;

        Collection<? extends GrantedAuthority> authorities = Arrays
                .stream(claims.get(AUTHORITIES_KEY).toString().split(","))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        SpringUser principal = new SpringUser(claims.getSubject(), "", authorities);
        String userLangKey = Optional.ofNullable(user.getLocaleKey()).orElse(Constants.DEFAULT_LANGUAGE);
        Locale.setDefault(Locale.forLanguageTag(userLangKey));
        return new UsernamePasswordAuthenticationToken(principal, token, authorities);

    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            LOGGER.info("Invalid JWT token.");
            LOGGER.trace("Invalid JWT token trace.", e);
        }
        return false;
    }
}
