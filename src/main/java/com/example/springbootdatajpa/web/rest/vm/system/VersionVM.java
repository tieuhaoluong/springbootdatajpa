package com.example.springbootdatajpa.web.rest.vm.system;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
public class VersionVM {
    @NotBlank
    @Length(max = 100)
    private String version;

    @NotBlank
    @Length(max = 1000)
    private String title;

    private String description;
}
