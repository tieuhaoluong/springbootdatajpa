package com.example.springbootdatajpa.web.rest.controller;

import com.example.springbootdatajpa.domain.User;
import com.example.springbootdatajpa.exception.NotFoundException;
import com.example.springbootdatajpa.security.AuthorizationToken;
import com.example.springbootdatajpa.security.jwt.JWTFilter;
import com.example.springbootdatajpa.security.jwt.JWTToken;
import com.example.springbootdatajpa.security.jwt.TokenProvider;
import com.example.springbootdatajpa.service.AuthService;
import com.example.springbootdatajpa.web.rest.vm.KeyAndPasswordVM;
import com.example.springbootdatajpa.web.rest.vm.LoginVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import java.util.Optional;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    private final AuthenticationManager authenticationManager;
    private final AuthService authService;
    private final TokenProvider tokenProvider;

    public AuthController(AuthenticationManager authenticationManager, AuthService authService, TokenProvider tokenProvider) {
        this.authenticationManager = authenticationManager;
        this.authService = authService;
        this.tokenProvider = tokenProvider;
    }

    @PostMapping("/login")
    public ResponseEntity<AuthorizationToken> login(@Valid @RequestBody LoginVM loginVM) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                loginVM.getUsername(),
                loginVM.getPassword()
        );

        Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = loginVM.isRememberMe();
        JWTToken jwtToken = tokenProvider.createToken(authentication, rememberMe);

        AuthorizationToken authorizationToken = new AuthorizationToken();
        authorizationToken.setId(jwtToken.getId());
        authorizationToken.setSubject(jwtToken.getSubject());
        authorizationToken.setAccessToken(jwtToken.getToken());
        authorizationToken.setExpiresAt(jwtToken.getExpiresAt());
        authorizationToken.setTokenType("Bearer");
        //authorizationToken.setRefreshToken();
        //authorizationToken.setScope();

        HttpHeaders httpHeaders = new HttpHeaders();
        String AUTHORIZATION_HEADER = authorizationToken.getTokenType() + " " + authorizationToken.getAccessToken();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, AUTHORIZATION_HEADER);

        return new ResponseEntity<>(authorizationToken, httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/reset-password/init")
    public void requestPasswordReset(@RequestParam @Email String email) {
        Optional<User> optionalUser = authService.requestPasswordReset(email);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            LOGGER.info("Password reset key: {}", user.getResetKey());
//            try {
//                AccountRecovery accountRecovery = new AccountRecovery(user.getLogin(), user.getResetKey());
//                emailService.sendEmailAccountRecovery(accountRecovery, user.getEmail(), user.getFullName());
//            } catch (UnsupportedEncodingException | MessagingException e) {
//                log.warn("EmailException.errorOccurredWhileSendingEmail", e.getCause());
//            }
        } else {
            // Pretend the request has been successful to prevent checking which emails really exist
            // but log that an invalid attempt has been made
            LOGGER.warn("Password reset requested for non existing email '{}'", email);
        }
    }

    @PostMapping("/reset-password/finish")
    public void finishPasswordReset(@Valid @RequestBody KeyAndPasswordVM keyAndPassword) {
        Optional<User> user = authService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey());
        if (user.isEmpty()) {
            throw new NotFoundException("No user was found for this reset key");
        }
    }

}
