package com.example.springbootdatajpa.web.rest.vm;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ChangePasswordVM extends PasswordVM {
    @NotBlank
    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String newPassword;

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
