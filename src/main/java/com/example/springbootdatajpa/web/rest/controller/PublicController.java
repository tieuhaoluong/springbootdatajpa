package com.example.springbootdatajpa.web.rest.controller;

import com.example.springbootdatajpa.domain.Language;
import com.example.springbootdatajpa.domain.Version;
import com.example.springbootdatajpa.repository.VersionRepository;
import com.example.springbootdatajpa.service.LanguageService;
import com.example.springbootdatajpa.web.rest.vm.system.LanguageVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/public")
public class PublicController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PublicController.class);

    private final LanguageService languageService;
    private final VersionRepository versionRepository;

    public PublicController(LanguageService languageService, VersionRepository versionRepository) {
        this.languageService = languageService;
        this.versionRepository = versionRepository;
    }

    @GetMapping("/versions")
    public ResponseEntity<List<Version>> findAllVersion() {
        LOGGER.debug("findAllVersion");
        return ResponseEntity.ok(versionRepository.findAll());
    }

    @GetMapping("/languages")
    public ResponseEntity<List<Language>> findAllLanguage() {
        LOGGER.debug("findAllLanguage");
        return ResponseEntity.ok(languageService.findAll());
    }

    @PostMapping("/languages")
    public ResponseEntity<Language> createLanguage(@Valid @RequestBody LanguageVM languageVM) {
        LOGGER.debug("createLanguage");
        Optional<Language> language = languageService.create(languageVM);
        return language.map(ResponseEntity::ok).orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @PutMapping("/languages/{id}")
    public ResponseEntity<Language> updateLanguage(@PathVariable UUID id, @Valid @RequestBody LanguageVM languageVM) {
        LOGGER.debug("updateLanguage");
        Optional<Language> language = languageService.update(id, languageVM);
        return language.map(ResponseEntity::ok).orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @DeleteMapping("/languages/{id}")
    public ResponseEntity<Language> deleteLanguage(@PathVariable UUID id) {
        LOGGER.debug("deleteLanguage");
        Optional<Language> language = languageService.delete(id);
        return language.map(ResponseEntity::ok).orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @GetMapping("/languages/{id}")
    public ResponseEntity<Language> findLanguageById(@PathVariable UUID id) {
        LOGGER.debug("findLanguageById");
        Optional<Language> language = languageService.findById(id);
        return language.map(ResponseEntity::ok).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
