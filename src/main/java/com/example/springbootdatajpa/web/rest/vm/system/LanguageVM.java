package com.example.springbootdatajpa.web.rest.vm.system;

import com.example.springbootdatajpa.constants.Constants;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@ToString
public class LanguageVM {
    @NotBlank
    @Pattern(regexp = Constants.Regex.LANGUAGE_KEY)
    @Length(max = 100)
    private String key;

    @NotBlank
    @Length(max = 100)
    private String locale;

    @NotNull
    @Length(max = 500)
    private String value;
}
