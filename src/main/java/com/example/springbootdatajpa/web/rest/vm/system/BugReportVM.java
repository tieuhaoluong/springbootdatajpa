package com.example.springbootdatajpa.web.rest.vm.system;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class BugReportVM {

    @NotBlank
    private String subject;

    @NotBlank
    private String description;
}
