package com.example.springbootdatajpa.web.rest.controller;

import com.example.springbootdatajpa.config.ApplicationProperties;
import com.example.springbootdatajpa.constants.Constants;
import com.example.springbootdatajpa.domain.Language;
import com.example.springbootdatajpa.domain.Version;
import com.example.springbootdatajpa.repository.LanguageRepository;
import com.example.springbootdatajpa.repository.VersionRepository;
import com.example.springbootdatajpa.web.rest.vm.system.LanguageVM;
import com.example.springbootdatajpa.web.rest.vm.system.VersionVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/system")
@Secured(Constants.Authorities.ROLE_SYSTEM)
public class SystemController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SystemController.class);

    private final ApplicationProperties applicationProperties;
    private final LanguageRepository languageRepository;
    private final VersionRepository versionRepository;

    public SystemController(ApplicationProperties applicationProperties, LanguageRepository languageRepository, VersionRepository versionRepository) {
        this.applicationProperties = applicationProperties;
        this.languageRepository = languageRepository;
        this.versionRepository = versionRepository;
    }

    @PostMapping("/versions")
    public ResponseEntity<Version> createVersion(@Valid @RequestBody VersionVM versionVM) {
        LOGGER.debug("createVersion");
        Version version = new Version();
        version.setVersion(versionVM.getVersion());
        version.setTitle(versionVM.getTitle());
        version.setDescription(versionVM.getDescription());
        versionRepository.save(version);
        return ResponseEntity.ok(version);
    }

    @PostMapping("/languages")
    public ResponseEntity<Language> createLanguage(@Valid @RequestBody LanguageVM languageVM) {
        LOGGER.debug("createLanguage");
        Language language = new Language();
        language.setKey(languageVM.getKey());
        language.setLocale(languageVM.getLocale());
        language.setValue(languageVM.getValue());
        languageRepository.save(language);
        return ResponseEntity.ok(language);
    }

    @GetMapping("/environment/{key}")
    public ResponseEntity<String> environment(@PathVariable String key) {
        LOGGER.debug("environment {}", key);
        return ResponseEntity.ok(applicationProperties.getEnvironment().getProperty(key));
    }

}
