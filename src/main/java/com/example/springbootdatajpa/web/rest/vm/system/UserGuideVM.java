package com.example.springbootdatajpa.web.rest.vm.system;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
public class UserGuideVM {

    @NotBlank
    private String subject;

    @NotBlank
    private String question;

    @NotBlank
    private String answer;

    @NotNull
    private Set<String> tags;
}
