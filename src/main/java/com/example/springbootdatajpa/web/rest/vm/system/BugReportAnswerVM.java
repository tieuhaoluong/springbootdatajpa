package com.example.springbootdatajpa.web.rest.vm.system;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class BugReportAnswerVM {
    @NotBlank
    private String answer;
}
