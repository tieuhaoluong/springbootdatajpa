package com.example.springbootdatajpa.constants.enumeration;

public enum UserGender {
    UNSET, MALE, FEMALE
}
