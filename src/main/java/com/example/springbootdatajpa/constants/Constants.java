package com.example.springbootdatajpa.constants;

/**
 * Application constants.
 */
public final class Constants {
    public static final String DEFAULT_LANGUAGE = "vi";

    private Constants() {
    }

    public final class UserLogin {
        public static final String SYSTEM = "system";
        public static final String ANONYMOUS_USER = "anonymousUser";
    }

    public final class Authorities {
        public static final String ROLE_SYSTEM = "ROLE_SYSTEM";
        public static final String ROLE_ADMIN = "ROLE_ADMIN";
        public static final String ROLE_USER = "ROLE_USER";
        public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
    }

    public final class SpringProfiles {
        public static final String DEVELOPMENT = "dev";
        public static final String PRODUCTION = "prod";
        public static final String CLOUD = "cloud";
    }

    public final class Regex {
        // Regex for acceptable login
        public static final String LOGIN = "^[_.@A-Za-z0-9-]*$";

        // Regex for acceptable language key
        public static final String LANGUAGE_KEY = "^[_.A-Za-z0-9]*$";
    }
}
