package com.example.springbootdatajpa.service;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Service
public class TranslateService {
    private final LocaleResolver localeResolver;
    private final MessageSource messageSource;
    private final HttpServletRequest request;

    public TranslateService(LocaleResolver localeResolver, MessageSource messageSource, HttpServletRequest request) {
        this.localeResolver = localeResolver;
        this.messageSource = messageSource;
        this.request = request;
    }

    public String get(String key) {
        return messageSource.getMessage(key, null, key, resolveLocale());
    }

    public String get(String key, Object... params) {
        return messageSource.getMessage(key, params, key, resolveLocale());
    }

    public String get(String key, Object[] params, String defaultMessage) {
        return messageSource.getMessage(key, params, defaultMessage, resolveLocale());
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    private Locale resolveLocale() {
        return localeResolver.resolveLocale(request);
    }

}
