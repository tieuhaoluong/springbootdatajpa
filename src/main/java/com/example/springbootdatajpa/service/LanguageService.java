package com.example.springbootdatajpa.service;

import com.example.springbootdatajpa.config.CacheConfiguration;
import com.example.springbootdatajpa.domain.Language;
import com.example.springbootdatajpa.exception.NotFoundException;
import com.example.springbootdatajpa.repository.LanguageRepository;
import com.example.springbootdatajpa.web.rest.vm.system.LanguageVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@EnableCaching
public class LanguageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(LanguageService.class);

    private final LanguageRepository languageRepository;
    private final CacheConfiguration cacheConfiguration;

    public LanguageService(LanguageRepository languageRepository, CacheConfiguration cacheConfiguration) {
        this.languageRepository = languageRepository;
        this.cacheConfiguration = cacheConfiguration;
    }

    @Transactional
    public Optional<Language> create(LanguageVM languageVM) {
        try {
            LOGGER.debug("Creating LanguageVM {}", languageVM);
            Language language = new Language();
            language.setKey(languageVM.getKey());
            language.setLocale(languageVM.getLocale());
            language.setValue(languageVM.getValue());

            saveWithCache(language);

            LOGGER.info("Created Language {}", language);
            return Optional.of(language);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Optional.empty();
        }
    }

    @Transactional
    public Optional<Language> update(UUID id, LanguageVM languageVM) {
        try {
            LOGGER.debug("Updating LanguageVM {}", languageVM);
            Language language = languageRepository.findById(id).orElse(null);
            if (language == null)
                throw new NotFoundException("Language not found");

            language.setKey(languageVM.getKey());
            language.setLocale(languageVM.getLocale());
            language.setValue(languageVM.getValue());
            language.setLastModifiedDate(Instant.now());

            saveWithCache(language);

            LOGGER.info("Updated Language {}", language);
            return Optional.of(language);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Optional.empty();
        }
    }

    @Transactional
    public Optional<Language> delete(UUID id) {
        try {
            LOGGER.debug("Deleting Language, id={}", id);
            Language language = languageRepository.findById(id).orElse(null);
            if (language == null)
                throw new NotFoundException("Language not found");

            deleteWithCache(language);

            LOGGER.info("Deleted Language {}", language);
            return Optional.of(language);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return Optional.empty();
        }
    }

    public Optional<Language> findById(UUID id) {
        return languageRepository.findById(id);
    }

    public Optional<Language> findByKeyAndLocale(String key, String locale) {
        return languageRepository.findByKeyAndLocale(key, locale);
    }

    public List<Language> findAll() {
        return languageRepository.findAll();
    }

    public List<Language> findAllByKey(String key) {
        return languageRepository.findAllByKey(key);
    }

    public Language saveWithCache(Language language) {
        LOGGER.debug("saveWithCache {}", language);
        language = languageRepository.save(language);
        cacheConfiguration.putLanguage(language);
        return language;
    }

    public void deleteWithCache(Language language) {
        LOGGER.debug("deleteWithCache {}", language);
        languageRepository.delete(language);
        cacheConfiguration.evictLanguage(language);
    }

}
