package com.example.springbootdatajpa.service;

import com.example.springbootdatajpa.domain.User;
import com.example.springbootdatajpa.exception.BadRequestException;
import com.example.springbootdatajpa.repository.UserRepository;
import com.example.springbootdatajpa.util.RandomUtils;
import com.example.springbootdatajpa.util.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthService {
    private final Logger LOGGER = LoggerFactory.getLogger(AuthService.class);

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final TranslateService translateService;

    public AuthService(UserRepository userRepository, PasswordEncoder passwordEncoder, TranslateService translateService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.translateService = translateService;
    }

    @Transactional
    public Optional<User> requestPasswordReset(String email) {
        LOGGER.debug("Request password reset for email '{}'", email);
        return userRepository.findByEmailIgnoreCase(email).filter(User::isActivated).map(user -> {
            user.setResetKey(RandomUtils.generateResetKey());
            user.setResetDate(Instant.now());
            userRepository.save(user);

            return user;
        });
    }

    @Transactional
    public Optional<User> completePasswordReset(String newPassword, String key) {
        LOGGER.debug("Reset user password for reset key {}", key);
        return userRepository.findByResetKey(key)
                .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
                .map(user -> {
                    user.setPassword(passwordEncoder.encode(newPassword));
                    user.setResetKey(null);
                    user.setResetDate(null);
                    userRepository.save(user);
                    return user;
                });
    }

    @Transactional
    public void changePassword(String currentClearTextPassword, String newPassword) {
        SecurityUtils.getCurrentUserLogin()
                .flatMap(userRepository::findByLogin)
                .ifPresent(user -> {
                    String currentEncryptedPassword = user.getPassword();
                    if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
                        throw new BadRequestException(translateService.get("BadRequestException.PasswordNotMatch"));
                    }
                    String encryptedPassword = passwordEncoder.encode(newPassword);
                    user.setPassword(encryptedPassword);
                    //this.clearUserCaches(user);
                    userRepository.save(user);

                    LOGGER.debug("Changed password for User: {}", user);
                });
    }

    private boolean removeNonActivatedUser(User existingUser) {
//        if (existingUser.isActivated()) {
//            return false;
//        }
//        userRepository.delete(existingUser);
//        userRepository.flush();
//        this.clearUserCaches(existingUser);
//        return true;
        return false;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public Optional<User> findById(UUID id) {
        return userRepository.findById(id);
    }

    public Optional<User> findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public Optional<User> findByEmailIgnoreCase(String email) {
        return userRepository.findByEmailIgnoreCase(email);
    }

//    public void updateAccount(UserUpdateVM userUpdateVM) {
//        log.debug("Update user account {}", userUpdateVM);
//        SecurityUtils.getCurrentUserLogin()
//                .flatMap(userRepository::findByLogin)
//                .ifPresent(user -> {
//                    userService.updateToUser(userUpdateVM, user);
//                    //this.clearUserCaches(user);
//                    userRepository.save(user);
//
//                    log.debug("Updated information for User: {}", user);
//                });
//    }
}
