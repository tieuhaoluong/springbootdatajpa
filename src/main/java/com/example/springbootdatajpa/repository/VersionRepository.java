package com.example.springbootdatajpa.repository;

import com.example.springbootdatajpa.domain.Version;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Spring Data JPA repository for the {@link Version} entity.
 */
@Repository
public interface VersionRepository extends JpaRepository<Version, UUID> {
}

