package com.example.springbootdatajpa.repository;

import com.example.springbootdatajpa.domain.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Spring Data JPA repository for the {@link Language} entity.
 */
@Repository
public interface LanguageRepository extends JpaRepository<Language, UUID> {
    List<Language> findAllByKey(String key);

    List<Language> findAllByKeyContaining(String key);

    Optional<Language> findByKeyAndLocale(String key, String locale);

    boolean existsByKeyAndLocale(String key, String locale);
}

