package com.example.springbootdatajpa.config;

import com.example.springbootdatajpa.domain.Language;
import com.example.springbootdatajpa.repository.LanguageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

@EnableCaching
@Configuration
public class CacheConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(CacheConfiguration.class);

    private final LanguageRepository languageRepository;

    public CacheConfiguration(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    @Cacheable(value = "tbl_language", key = "{ #key, #locale }")
    public Language findLanguage(String key, String locale) {
        LOGGER.debug("findLanguage key={}, locale={}", key, locale);
        return languageRepository.findByKeyAndLocale(key, locale).orElse(null);
    }

    @CachePut(value = "tbl_language", key = "{ #language.key, #language.locale }")
    public Language putLanguage(Language language) {
        LOGGER.debug("putLanguage {}", language);
        return language;
    }

    @CacheEvict(value = "tbl_language", key = "{ #language.key, #language.locale }")
    public void evictLanguage(Language language) {
        LOGGER.debug("evictLanguage {}", language);
    }
}
