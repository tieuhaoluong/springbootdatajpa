package com.example.springbootdatajpa.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.List;

@Configuration
public class ApplicationProperties {
    private final Environment environment;

    public ApplicationProperties(Environment environment) {
        this.environment = environment;
    }

    public List<String> getActiveProfiles() {
        return Arrays.asList(environment.getActiveProfiles());
    }

    public Environment getEnvironment() {
        return environment;
    }
}
