package com.example.springbootdatajpa.config;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.text.MessageFormat;
import java.util.Locale;

public class LocalMessageSource extends ReloadableResourceBundleMessageSource {

    @Override
    protected MessageFormat resolveCode(String code, Locale locale) {
        return super.resolveCode(code, locale);
    }
}
