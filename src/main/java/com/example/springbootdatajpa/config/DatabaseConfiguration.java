package com.example.springbootdatajpa.config;

import com.example.springbootdatajpa.constants.Constants;
import com.example.springbootdatajpa.constants.enumeration.UserGender;
import com.example.springbootdatajpa.domain.Authority;
import com.example.springbootdatajpa.domain.Language;
import com.example.springbootdatajpa.domain.User;
import com.example.springbootdatajpa.repository.AuthorityRepository;
import com.example.springbootdatajpa.repository.LanguageRepository;
import com.example.springbootdatajpa.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableJpaRepositories("com.example.springbootdatajpa.repository")
@EnableTransactionManagement
public class DatabaseConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseConfiguration.class);

    private final LanguageRepository languageRepository;
    private final AuthorityRepository authorityRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public DatabaseConfiguration(LanguageRepository languageRepository, AuthorityRepository authorityRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.languageRepository = languageRepository;
        this.authorityRepository = authorityRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    public void initDatabase() {
        createMessageI18n();
        createSystemAccount();
    }

    public void createAuthorities() {
        if (!authorityRepository.existsById(Constants.Authorities.ROLE_SYSTEM)) {
            Authority authority = new Authority();
            authority.setName(Constants.Authorities.ROLE_SYSTEM);
            authorityRepository.save(authority);
            LOGGER.debug("Created {}", authority);
        }
        if (!authorityRepository.existsById(Constants.Authorities.ROLE_ADMIN)) {
            Authority authority = new Authority();
            authority.setName(Constants.Authorities.ROLE_ADMIN);
            authorityRepository.save(authority);
            LOGGER.debug("Created {}", authority);
        }
        if (!authorityRepository.existsById(Constants.Authorities.ROLE_USER)) {
            Authority authority = new Authority();
            authority.setName(Constants.Authorities.ROLE_USER);
            authorityRepository.save(authority);
            LOGGER.debug("Created {}", authority);
        }
    }

    public void createSystemAccount() {
        Authority authority = authorityRepository
                .findById(Constants.Authorities.ROLE_SYSTEM)
                .orElseGet(() -> {
                    Authority roleSystem = new Authority();
                    roleSystem.setName(Constants.Authorities.ROLE_SYSTEM);
                    roleSystem = authorityRepository.save(roleSystem);
                    return roleSystem;
                });

        if (userRepository.findByLogin(Constants.UserLogin.SYSTEM).isEmpty()) {
            User user = new User();
            user.setLogin(Constants.UserLogin.SYSTEM);
            user.setPassword(passwordEncoder.encode(Constants.UserLogin.SYSTEM));
            user.setFullName(Constants.UserLogin.SYSTEM);
            user.setGender(UserGender.UNSET);
            user.setActivated(true);
            user.getAuthorities().add(authority);
            user = userRepository.save(user);
            LOGGER.debug("Created Account: {}", user.getLogin());
        }
        User system = userRepository.findByLogin(Constants.UserLogin.SYSTEM).orElseThrow();
        LOGGER.info("SYSTEM_ACCOUNT '{}' | ID '{}'", system.getLogin(), system.getId());
    }

    private void createMessageI18n() {
        List<Language> languages = new ArrayList<>();
        languages.add(createMessageLanguageObject("language", "vi", "Ngôn ngữ"));
        languages.add(createMessageLanguageObject("language", "en", "Language"));
        languages.add(createMessageLanguageObject("language.vi", "vi", "Tiếng Việt"));
        languages.add(createMessageLanguageObject("language.vi", "en", "Vietnamese"));
        languages.add(createMessageLanguageObject("language.en", "vi", "Tiếng Anh"));
        languages.add(createMessageLanguageObject("language.en", "en", "English"));

        languages.forEach(language -> {
            if (!languageRepository.existsByKeyAndLocale(language.getKey(), language.getLocale())) {
                languageRepository.save(language);
                LOGGER.info("Created Language: {} - {}", language.getLocale(), language.getKey());
            }
        });
    }

    private Language createMessageLanguageObject(String key, String lang, String value) {
        Language language = new Language();
        language.setKey(key);
        language.setLocale(lang);
        language.setValue(value);
        return language;
    }
}
