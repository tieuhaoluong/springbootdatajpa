package com.example.springbootdatajpa.config;

import com.example.springbootdatajpa.constants.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Locale;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    @Value("${management.endpoints.web.cors.allowed-origins:}")
    private String[] allowedOrigins;

    //    @Bean
//    public LocaleResolver localeResolver() {
//        Locale localeDefault = Locale.forLanguageTag(Constants.DEFAULT_LANGUAGE);
//        Locale.setDefault(localeDefault);
//        CookieLocaleResolver resolver = new CookieLocaleResolver();
//        resolver.setDefaultLocale(localeDefault);
//        resolver.setCookieName("locale");
//        resolver.setCookieMaxAge(30 * 24 * 60 * 60);
//        return resolver;
//    }

    @Bean
    public MessageSource messageSource() {
        return new CustomMessageSource(databaseMessageSource(), localMessageSource());
    }

    @Bean
    public LocalMessageSource localMessageSource() {
        LocalMessageSource messageSource = new LocalMessageSource();
        messageSource.setBasenames("classpath:i18n/messages", "classpath:i18n/ValidationMessages");
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setDefaultLocale(new Locale(Constants.DEFAULT_LANGUAGE));
        return messageSource;
    }

    @Bean
    public DatabaseMessageSource databaseMessageSource() {
        return new DatabaseMessageSource();
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns(allowedOrigins)
                .allowCredentials(true)
                .allowedMethods("*");
    }

//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(localeInterceptor());
//    }

}
