package com.example.springbootdatajpa.config;

import org.springframework.context.support.AbstractMessageSource;

import java.text.MessageFormat;
import java.util.Locale;

public class CustomMessageSource extends AbstractMessageSource {

    private final DatabaseMessageSource databaseMessageSource;
    private final LocalMessageSource localMessageSource;

    public CustomMessageSource(DatabaseMessageSource databaseMessageSource, LocalMessageSource localMessageSource) {
        this.databaseMessageSource = databaseMessageSource;
        this.localMessageSource = localMessageSource;
    }

    @Override
    protected MessageFormat resolveCode(String code, Locale locale) {
        MessageFormat messageFormat = databaseMessageSource.resolveCode(code, locale);
        if (messageFormat != null)
            return messageFormat;
        return localMessageSource.resolveCode(code, locale);
    }
}
