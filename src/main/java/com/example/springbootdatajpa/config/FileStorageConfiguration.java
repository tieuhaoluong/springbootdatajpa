package com.example.springbootdatajpa.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class FileStorageConfiguration {

    @Value("${app.storage-dir}")
    private String storageDir;

    public String getStorageDir() {
        return storageDir;
    }

    public Path getUploadDir() {
        return Paths.get(storageDir + "/upload/").toAbsolutePath().normalize();
    }
}
