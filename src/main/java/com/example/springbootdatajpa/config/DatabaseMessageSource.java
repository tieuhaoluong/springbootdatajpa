package com.example.springbootdatajpa.config;

import com.example.springbootdatajpa.domain.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractMessageSource;

import java.text.MessageFormat;
import java.util.Locale;

public class DatabaseMessageSource extends AbstractMessageSource {

    @Autowired
    private CacheConfiguration cacheConfiguration;

    @Override
    protected MessageFormat resolveCode(String code, Locale locale) {
        Language language = cacheConfiguration.findLanguage(code, locale.getLanguage());
        return language != null ? new MessageFormat(language.getValue(), locale) : null;
    }

}
