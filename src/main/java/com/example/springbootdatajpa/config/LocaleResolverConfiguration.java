package com.example.springbootdatajpa.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Configuration
public class LocaleResolverConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocaleResolverConfiguration.class);

    @Bean
    public LocaleResolver localeResolver() {
        return new SmartLocaleResolver();
    }

//    @Bean
//    public LocaleChangeInterceptor localeInterceptor() {
//        LocaleChangeInterceptor localeInterceptor = new LocaleChangeInterceptor();
//        localeInterceptor.setParamName("lang");
//        return localeInterceptor;
//    }

    public static class SmartLocaleResolver extends CookieLocaleResolver {
        @Override
        public Locale resolveLocale(HttpServletRequest request) {
            try {
                String frontendLanguage = request.getHeader("Client-Language");
                if (frontendLanguage != null && !frontendLanguage.trim().isEmpty()) {
                    request.setAttribute(CookieLocaleResolver.LOCALE_REQUEST_ATTRIBUTE_NAME, Locale.forLanguageTag(frontendLanguage));
                }
            } catch (Exception e) {
                LOGGER.debug("SmartLocaleResolver Error! Fallback to Locale default: {}", Locale.getDefault());
                return Locale.getDefault();
            }
            return super.resolveLocale(request);
        }
    }
}
